namespace MediaPurifier.Data.Entities;

public class MovieEntity : IEntity {
	// Primary Key
	public int MovieId { get; set; }

	// Columns
	public string Title { get; set; }
	public string? ImDbId { get; set; }
	public string? TvDbId { get; set; }

	// Relationships
	public SubtitleFileEntity SubtitleFile { get; set; }
	public ICollection<SubtitleEntity> Subtitles { get; set; }
}
