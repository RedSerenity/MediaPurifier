namespace MediaPurifier.Data.Entities;

public class SubtitleFileEntity : IEntity {
	// Primary Key
	public int SubtitleFileId { get; set; }

	// Columns
	public required string SubtitleFile { get; set; }

	// Relationships
	public int MovieId { get; set; }
	public MovieEntity Movie { get; set; }
	public ICollection<SubtitleEntity> Subtitles { get; set; }
}
