﻿using System.ComponentModel.DataAnnotations.Schema;

namespace MediaPurifier.Data.Entities;

public class SubtitleEntity : IEntity {
	// Primary Key
	public int SubtitleId { get; set; }

	// Columns
	public int BlockId { get; set; }
	public TimeSpan StartTime { get; set; }
	public TimeSpan EndTime { get; set; }

	[Column(TypeName = "varchar")]
	public string Text { get; set; }

	public bool HasProfanity { get; set; }

	// Relationships
	public int MovieId { get; set; }
	public MovieEntity Movie { get; set; }
	public int SubtitleFileId { get; set; }
	public SubtitleFileEntity SubtitleFile { get; set; }
}
