namespace MediaPurifier.Data.Entities;

public class ProfanityEntity : IEntity {
	// Primary Key
	public int ProfanityId { get; set; }

	// Columns
	public required string Profanity { get; set; }
	public string? Masked { get; set; }
}
