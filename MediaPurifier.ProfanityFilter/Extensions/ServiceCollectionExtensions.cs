using MediaPurifier.ProfanityFilter.Abstractions;
using MediaPurifier.ProfanityFilter.Services;
using Microsoft.Extensions.DependencyInjection;

namespace MediaPurifier.ProfanityFilter.Extensions;

public static class ServiceCollectionExtensions {
	public static IServiceCollection AddProfanityFilter(this IServiceCollection services) {
		return services
			.AddScoped<IProfanityService, ProfanityService>()
			.AddScoped<ProfanityFilter>();
	}
}
