using Microsoft.Extensions.Logging;

namespace MediaPurifier.ProfanityFilter;

public static class LogEventIds {
	public static readonly EventId ProfanityFilter = 5080;
}
