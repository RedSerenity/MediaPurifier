using MediaPurifier.Data.Entities;
using MediaPurifier.ProfanityFilter.Models;

namespace MediaPurifier.ProfanityFilter;

public static class Mappings {
	public static ProfanityEntity ToEntity(this Profanity model) {
		return new ProfanityEntity { ProfanityId = model.Id ?? 0, Profanity = model.Word, Masked = model.Masked };
	}

	public static Profanity ToModel(this ProfanityEntity entity) {
		return new Profanity { Id = entity.ProfanityId, Word = entity.Profanity, Masked = entity.Masked };
	}
}
