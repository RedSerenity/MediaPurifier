namespace MediaPurifier.ProfanityFilter;

public class ProfanityFilter {
	protected readonly Dictionary<string, string> Profanities = new();

	public int Count => Profanities.Count;

	#region Manipulate Profanities List
	public void LoadDefaultList() { LoadList(ProfanityList.WordList); }

	public void LoadList(Dictionary<string, string> profanityList) {
		ClearProfanityList();
		AddProfanityRange(profanityList);
	}

	public void AddProfanityRange(Dictionary<string, string> profanityList) {
		foreach (var key in profanityList.Keys) AddProfanity(key, profanityList[key]);
	}

	public void AddProfanity(string profanity, string censored) {
		Profanities.Add(
			profanity.ToLowerInvariant(),
			censored.ToLowerInvariant()
		);
	}

	public void RemoveProfanity(string profanity) { Profanities.Remove(profanity.ToLowerInvariant()); }

	public void ClearProfanityList() { Profanities.Clear(); }
	#endregion

	#region Profanity Methods
	public bool IsProfanity(string word) {
		return !string.IsNullOrEmpty(word) && Profanities.ContainsKey(word.ToLowerInvariant());
	}

	public bool SentenceContainsProfanity(string sentence) {
		var multiWord = sentence.Split(' ');

		foreach (var profanity in Profanities.Keys)
			foreach (var word in multiWord) {
				var completeWord = GetCompleteWord(word, profanity);

				if (completeWord is null) continue;

				return true;
			}

		return false;
	}

	private string? GetCompleteWord(string word, string profanity) {
		if (string.IsNullOrEmpty(word)) return null;

		word = word.ToLowerInvariant();

		if (!word.Contains(profanity)) return null;

		var startIndex = word.IndexOf(profanity, StringComparison.Ordinal);
		var endIndex = startIndex;

		// Work backwards in string to get to the start of the word.
		while (startIndex > 0) {
			if (word[startIndex - 1] == ' ' || char.IsPunctuation(word[startIndex - 1])) break;

			startIndex -= 1;
		}

		// Work forwards to get to the end of the word.
		while (endIndex < word.Length) {
			if (word[endIndex] == ' ' || char.IsPunctuation(word[endIndex])) break;

			endIndex += 1;
		}

		return word.Substring(startIndex, endIndex - startIndex);
	}
	#endregion
}
