using MediaPurifier.Data;
using MediaPurifier.Data.Entities;
using MediaPurifier.Data.Exceptions;
using MediaPurifier.ProfanityFilter.Abstractions;
using MediaPurifier.ProfanityFilter.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace MediaPurifier.ProfanityFilter.Services;

public class ProfanityService(ILogger<ProfanityService> logger, MediaPurifierContext dbContext) : IProfanityService {
	public IQueryable<Profanity> QueryProfanities() {
		return dbContext.Profanities
			.AsNoTracking()
			.Select(x => x.ToModel());
	}

	public async Task<Profanity> AddAsync(
		string profanity, string maskedProfanity,
		CancellationToken cancellationToken = default
	) {
		try {
			cancellationToken.ThrowIfCancellationRequested();

			var entity = new ProfanityEntity { Profanity = profanity, Masked = maskedProfanity };

			await dbContext.Profanities.AddAsync(entity, cancellationToken);
			await dbContext.SaveChangesAsync(cancellationToken);

			return entity.ToModel();
		} catch (OperationCanceledException) {
			logger.LogWarning(LogEventIds.ProfanityFilter, "Operation was cancelled");

			throw;
		}
	}

	public async Task<Profanity> GetAsync(int id, CancellationToken cancellationToken = default) {
		try {
			cancellationToken.ThrowIfCancellationRequested();

			var entity = await dbContext.Profanities.AsNoTracking()
				.SingleOrDefaultAsync(p => p.ProfanityId == id, cancellationToken);

			if (entity != null) return entity.ToModel();

			throw new EntityNotFoundException<ProfanityEntity>(id);
		} catch (OperationCanceledException) {
			logger.LogWarning(LogEventIds.ProfanityFilter, "Operation was cancelled");

			throw;
		}
	}

	public async Task<Profanity> GetByProfanityAsync(string profanity, CancellationToken cancellationToken = default) {
		try {
			cancellationToken.ThrowIfCancellationRequested();

			var entity = await dbContext.Profanities
				.AsNoTracking()
				.SingleOrDefaultAsync(p => p.Profanity == profanity, cancellationToken);

			if (entity != null) return entity.ToModel();

			throw new EntityNotFoundException<ProfanityEntity>(profanity);
		} catch (OperationCanceledException) {
			logger.LogWarning(LogEventIds.ProfanityFilter, "Operation was cancelled");

			throw;
		}
	}

	public async Task<IEnumerable<Profanity>> ListAsync(CancellationToken cancellationToken = default) {
		try {
			cancellationToken.ThrowIfCancellationRequested();

			var entities = await dbContext.Profanities
				.AsNoTracking()
				.ToListAsync(cancellationToken);

			return entities.Select(entity => entity.ToModel());
		} catch (OperationCanceledException) {
			logger.LogWarning(LogEventIds.ProfanityFilter, "Operation was cancelled");

			throw;
		}
	}

	public async Task<bool> RemoveAsync(int id, CancellationToken cancellationToken = default) {
		try {
			cancellationToken.ThrowIfCancellationRequested();

			var entity = await dbContext.Profanities.SingleOrDefaultAsync(x => x.ProfanityId == id, cancellationToken);

			if (entity == null) {
				logger.LogWarning(LogEventIds.ProfanityFilter, "Could not find ProfanityEntity by Id {Id}", id);

				throw new EntityNotFoundException<ProfanityEntity>(id);
			}

			dbContext.Profanities.Remove(entity);
			await dbContext.SaveChangesAsync(cancellationToken);

			return true;
		} catch (OperationCanceledException) {
			logger.LogWarning(LogEventIds.ProfanityFilter, "Operation was cancelled");

			throw;
		}
	}
}
