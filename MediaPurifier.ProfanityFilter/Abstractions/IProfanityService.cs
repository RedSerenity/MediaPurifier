using MediaPurifier.ProfanityFilter.Models;

namespace MediaPurifier.ProfanityFilter.Abstractions;

public interface IProfanityService {
	IQueryable<Profanity> QueryProfanities();
	Task<Profanity> AddAsync(string profanity, string maskedProfanity, CancellationToken cancellationToken = default);
	Task<Profanity> GetAsync(int id, CancellationToken cancellationToken = default);
	Task<Profanity> GetByProfanityAsync(string profanity, CancellationToken cancellationToken = default);
	Task<IEnumerable<Profanity>> ListAsync(CancellationToken cancellationToken = default);
	Task<bool> RemoveAsync(int id, CancellationToken cancellationToken = default);
}
