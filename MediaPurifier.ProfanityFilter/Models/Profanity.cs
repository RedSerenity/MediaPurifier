namespace MediaPurifier.ProfanityFilter.Models;

public record Profanity {
	public int? Id { get; init; }
	public required string Word { get; init; }
	public string? Masked { get; init; }

	public string GetMaskedWord() { return Masked ?? Word; }
}
