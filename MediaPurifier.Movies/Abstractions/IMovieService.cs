using MediaPurifier.Movies.Models;

namespace MediaPurifier.Movies.Abstractions;

public interface IMovieService {
	IQueryable<Movie> QueryMovies();
	Task<Movie> AddAsync(string title, string? imdbId, string? tvdbId, CancellationToken cancellationToken = default);
	Task<Movie> GetAsync(int id, CancellationToken cancellationToken = default);
	Task<List<Movie>> ListAsync(CancellationToken cancellationToken = default);

	Task<Movie> UpdateAsync(
		int id, string title, string? imdbId, string? tvdbId,
		CancellationToken cancellationToken = default
	);

	Task<bool> DeleteAsync(int id, CancellationToken cancellationToken = default);
}
