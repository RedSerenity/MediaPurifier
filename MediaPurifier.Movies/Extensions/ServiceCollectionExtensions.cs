using MediaPurifier.Movies.Abstractions;
using MediaPurifier.Movies.Services;
using Microsoft.Extensions.DependencyInjection;

namespace MediaPurifier.Movies.Extensions;

public static class ServiceCollectionExtensions {
	public static IServiceCollection AddMovies(this IServiceCollection services) {
		return services
			.AddScoped<IMovieService, MovieService>();
	}
}
