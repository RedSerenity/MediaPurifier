using MediaPurifier.Data.Entities;
using MediaPurifier.Movies.Models;

namespace MediaPurifier.Movies;

public static class Mappings {
	public static MovieEntity ToEntity(this Movie rec) {
		return new MovieEntity { MovieId = rec.Id ?? 0, Title = rec.Title, ImDbId = rec.ImDbId, TvDbId = rec.TvDbId };
	}

	public static Movie ToModel(this MovieEntity entity) {
		return new Movie { Id = entity.MovieId, Title = entity.Title, ImDbId = entity.ImDbId, TvDbId = entity.TvDbId };
	}
}
