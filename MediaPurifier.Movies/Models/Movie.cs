namespace MediaPurifier.Movies.Models;

public record Movie {
	public int? Id { get; init; }
	public required string Title { get; init; }
	public string? ImDbId { get; init; }
	public string? TvDbId { get; init; }
}
