﻿using Microsoft.Extensions.Logging;

namespace MediaPurifier.Movies;

public static class LogEventIds {
	public static readonly EventId Movies = 5100;
}
