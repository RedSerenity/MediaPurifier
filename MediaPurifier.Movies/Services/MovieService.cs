using MediaPurifier.Data;
using MediaPurifier.Data.Entities;
using MediaPurifier.Data.Exceptions;
using MediaPurifier.Movies.Abstractions;
using MediaPurifier.Movies.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace MediaPurifier.Movies.Services;

public class MovieService(ILogger<MovieService> logger, MediaPurifierContext dbContext) : IMovieService {
	public IQueryable<Movie> QueryMovies() {
		return dbContext.Movies
			.AsNoTracking()
			.Select(x => x.ToModel());
	}

	public async Task<Movie> AddAsync(
		string title, string? imdbId, string? tvdbId,
		CancellationToken cancellationToken = default
	) {
		try {
			cancellationToken.ThrowIfCancellationRequested();

			var entity = new MovieEntity {
				Title = title,
				ImDbId = imdbId,
				TvDbId = tvdbId
			};

			await dbContext.Movies.AddAsync(entity, cancellationToken);
			await dbContext.SaveChangesAsync(cancellationToken);

			return entity.ToModel();
		} catch (OperationCanceledException) {
			logger.LogWarning(LogEventIds.Movies, "Operation was cancelled");

			throw;
		}
	}

	public async Task<Movie> GetAsync(int id, CancellationToken cancellationToken = default) {
		try {
			cancellationToken.ThrowIfCancellationRequested();

			var entity = await dbContext.Movies.SingleOrDefaultAsync(x => x.MovieId == id, cancellationToken);

			if (entity == null) {
				logger.LogWarning(LogEventIds.Movies, "Movie with id {Id} not found", id);

				throw new EntityNotFoundException<MovieEntity>(id);
			}

			return entity.ToModel();
		} catch (OperationCanceledException) {
			logger.LogWarning(LogEventIds.Movies, "Operation was cancelled");

			throw;
		}
	}

	public async Task<List<Movie>> ListAsync(CancellationToken cancellationToken = default) {
		try {
			cancellationToken.ThrowIfCancellationRequested();

			var entities = await dbContext.Movies.ToListAsync(cancellationToken);

			return entities
				.Select(entity => entity.ToModel())
				.ToList();
		} catch (OperationCanceledException) {
			logger.LogWarning(LogEventIds.Movies, "Operation was cancelled");

			throw;
		}
	}

	public async Task<Movie> UpdateAsync(
		int id, string title, string? imdbId, string? tvdbId,
		CancellationToken cancellationToken = default
	) {
		try {
			cancellationToken.ThrowIfCancellationRequested();

			var entity = await dbContext.Movies.SingleOrDefaultAsync(x => x.MovieId == id, cancellationToken);

			if (entity == null) {
				logger.LogWarning(LogEventIds.Movies, "Movie with Id {Id} not found", id);

				throw new EntityNotFoundException<MovieEntity>(id);
			}

			entity.Title = title;
			entity.ImDbId = imdbId;
			entity.TvDbId = tvdbId;

			await dbContext.SaveChangesAsync(cancellationToken);

			return entity.ToModel();
		} catch (OperationCanceledException) {
			logger.LogWarning(LogEventIds.Movies, "Operation was cancelled");

			throw;
		}
	}

	public async Task<bool> DeleteAsync(int id, CancellationToken cancellationToken = default) {
		try {
			cancellationToken.ThrowIfCancellationRequested();

			var entity = await dbContext.Movies.SingleOrDefaultAsync(x => x.MovieId == id, cancellationToken);

			if (entity == null) {
				logger.LogWarning(LogEventIds.Movies, "Movie with Id {Id} not found", id);

				throw new EntityNotFoundException<MovieEntity>(id);
			}

			dbContext.Movies.Remove(entity);
			await dbContext.SaveChangesAsync(cancellationToken);

			return true;
		} catch (OperationCanceledException) {
			logger.LogWarning(LogEventIds.Movies, "Operation was cancelled");

			throw;
		}
	}
}
