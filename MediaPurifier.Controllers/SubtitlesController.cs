﻿using System.Text;
using MediaPurifier.Data;
using MediaPurifier.Data.Entities;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace MediaPurifier.Controllers;

[ApiController]
[Route("movies/{movieId:int}/subtitle")]
public class SubtitlesController(MediaPurifierContext dbContext) : ControllerBase {
	[HttpPost]
	public async Task<IResult> Upload(int movieId, IFormFile file, CancellationToken cancellationToken) {
		if (file.Length == 0) return Results.BadRequest("File is required");

		using var stream = new MemoryStream();
		await file.CopyToAsync(stream, cancellationToken);

		var fileContents = Encoding.UTF8
			.GetString(stream.ToArray())
			.ReplaceLineEndings("\n");

		var entity = new SubtitleFileEntity {
			MovieId = movieId,
			SubtitleFile = fileContents
		};

		if (entity.SubtitleFile.Length == 0) return Results.BadRequest("File is empty");

		await dbContext.SubtitleFiles.AddAsync(entity, cancellationToken);
		await dbContext.SaveChangesAsync(cancellationToken);

		return Results.Ok();
	}
}
