using MediaPurifier.ProfanityFilter.Abstractions;
using MediaPurifier.ProfanityFilter.Models;

namespace MediaPurifier.GraphQL;

public partial class Queries {
	[UseSingleOrDefault]
	[UseProjection]
	[UseFiltering]
	public IQueryable<Profanity> GetBadWord(IProfanityService service) =>
		service.QueryProfanities();

	[UsePaging]
	[UseProjection]
	[UseFiltering]
	[UseSorting]
	public IQueryable<Profanity> GetBadWords(IProfanityService service) =>
		service.QueryProfanities();
}
