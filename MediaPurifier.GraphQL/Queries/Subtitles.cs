using MediaPurifier.Subtitles.Abstractions;
using MediaPurifier.Subtitles.Models;

namespace MediaPurifier.GraphQL;

public partial class Queries {
	[UseSingleOrDefault]
	[UseProjection]
	[UseFiltering]
	public IQueryable<SubtitleFile> GetSubtitleFile(ISubtitleFileService service) =>
		service.QuerySubtitleFile();

	[UsePaging]
	[UseProjection]
	[UseFiltering]
	[UseSorting]
	public IQueryable<SubtitleFile> GetSubtitleFiles(ISubtitleFileService service) =>
		service.QuerySubtitleFile();

	[UseSingleOrDefault]
	[UseProjection]
	[UseFiltering]
	public IQueryable<Subtitle> GetSubtitle(ISubtitleService service) =>
		service.QuerySubtitles();

	[UsePaging]
	[UseProjection]
	[UseFiltering]
	[UseSorting]
	public IQueryable<Subtitle> GetSubtitles(ISubtitleService service) =>
		service.QuerySubtitles();
}
