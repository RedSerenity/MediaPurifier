using MediaPurifier.FileBrowser.Abstractions;
using MediaPurifier.FileBrowser.Models;

namespace MediaPurifier.GraphQL;

public partial class Queries {
	public async Task<List<FileRecord>> GetFiles(string path, string? filter, IFileService service) =>
		filter is null ?
			await service.ListFilesAsync(path) :
			await service.ListFilesFilterAsync(path, filter);
}
