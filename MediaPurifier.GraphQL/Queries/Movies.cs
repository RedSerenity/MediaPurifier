using MediaPurifier.Movies.Abstractions;
using MediaPurifier.Movies.Models;

namespace MediaPurifier.GraphQL;

public partial class Queries {
	[UseSingleOrDefault]
	[UseProjection]
	[UseFiltering]
	public IQueryable<Movie> GetMovie(IMovieService service) =>
		service.QueryMovies();

	[UsePaging]
	[UseProjection]
	[UseFiltering]
	[UseSorting]
	public IQueryable<Movie> GetMovies(IMovieService service) =>
		service.QueryMovies();
}
