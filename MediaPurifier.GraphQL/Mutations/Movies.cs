using MediaPurifier.GraphQL.Types.Movies;
using MediaPurifier.Movies.Abstractions;
using MediaPurifier.Movies.Models;

namespace MediaPurifier.GraphQL;

public partial class Mutations {
	public async Task<Movie> AddMovie(AddMovieInput input, IMovieService service, CancellationToken cancellationToken) {
		try {
			return await service.AddAsync(input.Title, input.ImDbId, input.TvDbId, cancellationToken);
		} catch (Exception ex) {
			throw new GraphQLException("Unable to add movie", ex);
		}
	}

	public async Task<Movie> UpdateMovie(
		UpdateMovieInput input, IMovieService service,
		CancellationToken cancellationToken
	) {
		try {
			return await service.UpdateAsync(input.Id, input.Title, input.ImDbId, input.TvDbId, cancellationToken);
		} catch (Exception ex) {
			throw new GraphQLException($"Unable to update movie {input.Id}", ex);
		}
	}

	public async Task<bool> DeleteMovie([ID] int movieId, IMovieService service, CancellationToken cancellationToken) {
		try {
			return await service.DeleteAsync(movieId, cancellationToken);
		} catch (Exception ex) {
			throw new GraphQLException($"Unable to delete movie {movieId}", ex);
		}
	}
}
