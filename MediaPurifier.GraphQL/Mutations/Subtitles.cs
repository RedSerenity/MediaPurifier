using MediaPurifier.GraphQL.Types.Subtitles;
using MediaPurifier.Subtitles.Abstractions;
using MediaPurifier.Subtitles.Models;

namespace MediaPurifier.GraphQL;

public partial class Mutations {
	public async Task<SubtitleFile> AddSubtitleFile(
		AddSubtitleFileInput fileInput, ISubtitleFileService service,
		CancellationToken cancellationToken
	) {
		try {
			return await service.AddAsync(fileInput.MovieId, fileInput.Subtitle, cancellationToken);
		} catch (Exception ex) {
			throw new GraphQLException("Unable to add subtitle file", ex);
		}
	}

	public async Task<SubtitleFile> UpdateSubtitleFile(
		UpdateSubtitleFileInput fileInput, ISubtitleFileService service,
		CancellationToken cancellationToken
	) {
		try {
			return await service.UpdateAsync(
				new SubtitleFile { Id = fileInput.Id, File = fileInput.Subtitle },
				cancellationToken
			);
		} catch (Exception ex) {
			throw new GraphQLException($"Unable to update subtitle file {fileInput.Id}", ex);
		}
	}

	public async Task<bool> ParseSubtitleFile(
		[ID] int id, ISubtitleFileService service,
		CancellationToken cancellationToken
	) {
		try {
			return await service.ParseFileAsync(id, cancellationToken);
		} catch (Exception ex) {
			throw new GraphQLException("Unable to parse subtitle file into blocks", ex);
		}
	}

	public async Task<bool> DeleteSubtitleFile(
		[ID] int id, ISubtitleFileService service,
		CancellationToken cancellationToken
	) {
		try {
			return await service.DeleteAsync(id, cancellationToken);
		} catch (Exception ex) {
			throw new GraphQLException($"Unable to delete subtitle file {id}", ex);
		}
	}

	public async Task<Subtitle> AddSubtitle(ISubtitleService service, CancellationToken cancellationToken) {
		try {
			//return await service.AddAsync();
			throw new NotImplementedException();
		} catch (Exception ex) {
			throw new GraphQLException("Unable to add subtitle", ex);
		}
	}
}
