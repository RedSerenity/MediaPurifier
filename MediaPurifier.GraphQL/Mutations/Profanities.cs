using MediaPurifier.GraphQL.Types.Profanities;
using MediaPurifier.ProfanityFilter.Abstractions;
using MediaPurifier.ProfanityFilter.Models;

namespace MediaPurifier.GraphQL;

public partial class Mutations {
	public async Task<Profanity?> AddProfanity(
		AddProfanityInput input, IProfanityService service,
		CancellationToken cancellationToken
	) {
		try {
			return await service.AddAsync(input.Word, input.MaskedWord, cancellationToken);
		} catch (Exception ex) {
			throw new GraphQLException("Unable to add profanity", ex);
		}
	}

	public async Task<bool> DeleteProfanity(
		[ID] int profanityId, IProfanityService service,
		CancellationToken cancellationToken
	) {
		try {
			return await service.RemoveAsync(profanityId, cancellationToken);
		} catch (Exception ex) {
			throw new GraphQLException($"Unable to delete profanity {profanityId}", ex);
		}
	}
}
