using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Routing;

namespace MediaPurifier.GraphQL.Extensions;

public static class EndpointRouteBuilderExtensions {
	public static IEndpointRouteBuilder MapMpGraphQL(this IEndpointRouteBuilder endpoints) {
		endpoints.MapGraphQL();

		return endpoints;
	}
}
