using MediaPurifier.FileBrowser.Abstractions;
using MediaPurifier.Movies.Abstractions;
using MediaPurifier.ProfanityFilter.Abstractions;
using MediaPurifier.Subtitles.Abstractions;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;

namespace MediaPurifier.GraphQL.Extensions;

public static class ServiceCollectionExtensions {
	public static IServiceCollection AddMpGraphQL<TDbContext>(this IServiceCollection services)
		where TDbContext : DbContext {
		services
			.AddGraphQLServer()
			
			// Register DB Context
			.RegisterDbContext<TDbContext>()
			
			// Register Services
			.RegisterService<IMovieService>()
			.RegisterService<IProfanityService>()
			.RegisterService<ISubtitleService>()
			.RegisterService<ISubtitleFileService>()
			.RegisterService<IFileService>()
			
			// Add Query/Mutation
			.AddQueryType<Queries>()
			.AddMutationType<Mutations>()
			
			// Add Query Helpers
			.AddProjections()
			.AddFiltering()
			.AddSorting();

		return services;
	}
}
