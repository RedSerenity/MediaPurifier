﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace MediaPurifier.Data.Migrations
{
    /// <inheritdoc />
    public partial class Migration3 : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "ImdbId",
                table: "Movies",
                newName: "ImDbId");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "ImDbId",
                table: "Movies",
                newName: "ImdbId");
        }
    }
}
