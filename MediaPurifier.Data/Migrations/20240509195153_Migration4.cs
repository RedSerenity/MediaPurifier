﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace MediaPurifier.Data.Migrations
{
    /// <inheritdoc />
    public partial class Migration4 : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<bool>(
                name: "HasBadWord",
                table: "Subtitles",
                type: "boolean",
                nullable: false,
                defaultValue: false);

            migrationBuilder.AddColumn<string>(
                name: "MaskedWord",
                table: "BadWords",
                type: "text",
                nullable: false,
                defaultValue: "");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "HasBadWord",
                table: "Subtitles");

            migrationBuilder.DropColumn(
                name: "MaskedWord",
                table: "BadWords");
        }
    }
}
