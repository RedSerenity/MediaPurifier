﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;
using Npgsql.EntityFrameworkCore.PostgreSQL.Metadata;

#nullable disable

namespace MediaPurifier.Data.Migrations
{
    /// <inheritdoc />
    public partial class NextMigration : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<int>(
                name: "BlockId",
                table: "Subtitles",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<TimeSpan>(
                name: "EndTime",
                table: "Subtitles",
                type: "interval",
                nullable: false,
                defaultValue: new TimeSpan(0, 0, 0, 0, 0));

            migrationBuilder.AddColumn<int>(
                name: "RawSubtitleId",
                table: "Subtitles",
                type: "integer",
                nullable: false,
                defaultValue: 0);

            migrationBuilder.AddColumn<TimeSpan>(
                name: "StartTime",
                table: "Subtitles",
                type: "interval",
                nullable: false,
                defaultValue: new TimeSpan(0, 0, 0, 0, 0));

            migrationBuilder.AddColumn<string>(
                name: "Text",
                table: "Subtitles",
                type: "varchar",
                nullable: false,
                defaultValue: "");

            migrationBuilder.CreateTable(
                name: "BadWords",
                columns: table => new
                {
                    BadWordId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Word = table.Column<string>(type: "text", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_BadWords", x => x.BadWordId);
                });

            migrationBuilder.CreateTable(
                name: "RawSubtitles",
                columns: table => new
                {
                    RawSubtitleId = table.Column<int>(type: "integer", nullable: false)
                        .Annotation("Npgsql:ValueGenerationStrategy", NpgsqlValueGenerationStrategy.IdentityByDefaultColumn),
                    Subtitle = table.Column<string>(type: "text", nullable: false),
                    MovieId = table.Column<int>(type: "integer", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_RawSubtitles", x => x.RawSubtitleId);
                    table.ForeignKey(
                        name: "FK_RawSubtitles_Movies_MovieId",
                        column: x => x.MovieId,
                        principalTable: "Movies",
                        principalColumn: "MovieId",
                        onDelete: ReferentialAction.Cascade);
                });

            migrationBuilder.CreateIndex(
                name: "IX_Subtitles_MovieId",
                table: "Subtitles",
                column: "MovieId");

            migrationBuilder.CreateIndex(
                name: "IX_Subtitles_RawSubtitleId",
                table: "Subtitles",
                column: "RawSubtitleId");

            migrationBuilder.CreateIndex(
                name: "IX_RawSubtitles_MovieId",
                table: "RawSubtitles",
                column: "MovieId",
                unique: true);

            migrationBuilder.AddForeignKey(
                name: "FK_Subtitles_Movies_MovieId",
                table: "Subtitles",
                column: "MovieId",
                principalTable: "Movies",
                principalColumn: "MovieId",
                onDelete: ReferentialAction.Cascade);

            migrationBuilder.AddForeignKey(
                name: "FK_Subtitles_RawSubtitles_RawSubtitleId",
                table: "Subtitles",
                column: "RawSubtitleId",
                principalTable: "RawSubtitles",
                principalColumn: "RawSubtitleId",
                onDelete: ReferentialAction.Cascade);
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropForeignKey(
                name: "FK_Subtitles_Movies_MovieId",
                table: "Subtitles");

            migrationBuilder.DropForeignKey(
                name: "FK_Subtitles_RawSubtitles_RawSubtitleId",
                table: "Subtitles");

            migrationBuilder.DropTable(
                name: "BadWords");

            migrationBuilder.DropTable(
                name: "RawSubtitles");

            migrationBuilder.DropIndex(
                name: "IX_Subtitles_MovieId",
                table: "Subtitles");

            migrationBuilder.DropIndex(
                name: "IX_Subtitles_RawSubtitleId",
                table: "Subtitles");

            migrationBuilder.DropColumn(
                name: "BlockId",
                table: "Subtitles");

            migrationBuilder.DropColumn(
                name: "EndTime",
                table: "Subtitles");

            migrationBuilder.DropColumn(
                name: "RawSubtitleId",
                table: "Subtitles");

            migrationBuilder.DropColumn(
                name: "StartTime",
                table: "Subtitles");

            migrationBuilder.DropColumn(
                name: "Text",
                table: "Subtitles");
        }
    }
}
