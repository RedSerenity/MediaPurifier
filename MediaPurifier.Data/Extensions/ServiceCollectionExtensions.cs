using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Diagnostics;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;

namespace MediaPurifier.Data.Extensions;

public static class ServiceCollectionExtensions {
	public static IServiceCollection AddDatabase(this IServiceCollection service, IConfiguration configuration) {
		var dataAssemblyName = typeof(ServiceCollectionExtensions).Assembly.GetName();
		service.AddDbContext<MediaPurifierContext>(
			options => {
				options
					.UseNpgsql(
						configuration.GetConnectionString("DefaultConnection"),
						x => x.MigrationsAssembly(dataAssemblyName.Name)
					);

				options.ConfigureWarnings(
					c =>
						c.Log(
							(RelationalEventId.ConnectionCreating, LogLevel.Trace),
							(RelationalEventId.ConnectionCreated, LogLevel.Trace),
							(RelationalEventId.ConnectionOpening, LogLevel.Trace),
							(RelationalEventId.ConnectionOpened, LogLevel.Trace),
							(RelationalEventId.ConnectionClosing, LogLevel.Trace),
							(RelationalEventId.ConnectionClosed, LogLevel.Trace),
							(RelationalEventId.ConnectionDisposing, LogLevel.Trace),
							(RelationalEventId.ConnectionDisposed, LogLevel.Trace),
							(RelationalEventId.DataReaderClosing, LogLevel.Trace),
							(RelationalEventId.DataReaderDisposing, LogLevel.Trace),
							(RelationalEventId.CommandCreating, LogLevel.Trace),
							(RelationalEventId.CommandCreated, LogLevel.Trace),
							(RelationalEventId.CommandInitialized, LogLevel.Trace),
							(RelationalEventId.CommandExecuting, LogLevel.Trace),
							(RelationalEventId.CommandExecuted, LogLevel.Trace),
							(RelationalEventId.CommandCanceled, LogLevel.Trace)
						)
				);
			}
		);

		return service;
	}
}
