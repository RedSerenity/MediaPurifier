using MediaPurifier.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace MediaPurifier.Data.Configurations;

public class MovieConfiguration : IEntityTypeConfiguration<MovieEntity> {
	public void Configure(EntityTypeBuilder<MovieEntity> builder) {
		builder.HasKey(e => e.MovieId);

		builder
			.HasMany(e => e.Subtitles)
			.WithOne(e => e.Movie)
			.HasForeignKey(e => e.MovieId);

		builder
			.HasOne(e => e.SubtitleFile)
			.WithOne(e => e.Movie)
			.HasForeignKey<SubtitleFileEntity>(e => e.MovieId);
	}
}
