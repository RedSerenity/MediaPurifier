using MediaPurifier.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace MediaPurifier.Data.Configurations;

public class BadWordConfiguration : IEntityTypeConfiguration<ProfanityEntity> {
	public void Configure(EntityTypeBuilder<ProfanityEntity> builder) { builder.HasKey(e => e.ProfanityId); }
}
