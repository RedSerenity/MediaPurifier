using MediaPurifier.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace MediaPurifier.Data.Configurations;

public class SubtitleConfiguration : IEntityTypeConfiguration<SubtitleEntity> {
	public void Configure(EntityTypeBuilder<SubtitleEntity> builder) {
		builder.HasKey(e => e.SubtitleId);

		builder
			.Property(p => p.HasProfanity)
			.HasDefaultValue(false);
	}
}
