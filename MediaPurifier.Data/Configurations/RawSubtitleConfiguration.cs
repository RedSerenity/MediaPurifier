using MediaPurifier.Data.Entities;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata.Builders;

namespace MediaPurifier.Data.Configurations;

public class RawSubtitleConfiguration : IEntityTypeConfiguration<SubtitleFileEntity> {
	public void Configure(EntityTypeBuilder<SubtitleFileEntity> builder) {
		builder.HasKey(e => e.SubtitleFileId);

		builder
			.HasMany(e => e.Subtitles)
			.WithOne(e => e.SubtitleFile)
			.HasForeignKey(e => e.SubtitleFileId);
	}
}
