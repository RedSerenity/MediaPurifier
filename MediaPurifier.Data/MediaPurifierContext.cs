﻿using MediaPurifier.Data.Entities;
using Microsoft.EntityFrameworkCore;

namespace MediaPurifier.Data;

public class MediaPurifierContext(DbContextOptions<MediaPurifierContext> options) : DbContext(options) {
	public DbSet<ProfanityEntity> Profanities { get; set; }
	public DbSet<MovieEntity> Movies { get; set; }
	public DbSet<SubtitleFileEntity> SubtitleFiles { get; set; }
	public DbSet<SubtitleEntity> Subtitles { get; set; }

	protected override void OnModelCreating(ModelBuilder modelBuilder) {
		modelBuilder.ApplyConfigurationsFromAssembly(typeof(MediaPurifierContext).Assembly);
	}
}
