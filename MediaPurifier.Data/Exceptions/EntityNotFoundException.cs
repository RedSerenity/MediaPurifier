using MediaPurifier.Data.Entities;

namespace MediaPurifier.Data.Exceptions;

public class EntityNotFoundException<TEntity> : Exception
	where TEntity : IEntity {
	public EntityNotFoundException(int id) : base($"Could not locate '{typeof(TEntity).Name}' with ID {id}") { }

	public EntityNotFoundException(int? id) : base($"Could not locate '{typeof(TEntity).Name}' with ID {id}") { }

	public EntityNotFoundException(string key) : base($"Could not locate '{typeof(TEntity).Name}' with KEY {key}") { }
}
