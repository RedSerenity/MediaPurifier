import { ChangeDetectionStrategy, ChangeDetectorRef, Component, OnDestroy, OnInit } from "@angular/core";
import { NbButtonModule, NbCardModule, NbIconModule, NbListModule, NbStepperModule } from "@nebular/theme";
import { delay, filter, map, Subject, take } from "rxjs";
import { CommonModule } from "@angular/common";

import { FileRecord, GraphQLAccessor } from "@Libraries/GraphQL";
import { LoadingSpinnerComponent } from "@Libraries/UI";

import { LIST_DIR_QUERY } from "./Queries";
import { FileRecordRequest } from "./Types";
import { endSpinner, startSpinner } from "@Libraries/UI/LoadingSpinner";
import { NgxSpinnerService } from "ngx-spinner";

const ROOT_PATH = '/';
const PARENT_PATH = '..';

@Component({
  selector: 'mp-wizard-step-select-movie',
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [
    NbButtonModule,
    NbCardModule,
    NbStepperModule,
    NbListModule,
    CommonModule,
    NbIconModule,
    LoadingSpinnerComponent
  ],
  styleUrl: './WizardStepSelectMovie.component.scss',
  template: `
    <nb-card>
      <mp-loading-spinner name="wizard1"></mp-loading-spinner>
      <nb-card-header>Select Movie - {{currentPath}}</nb-card-header>
      <nb-card-body>
        <nb-list>
          <nb-list-item *ngFor="let file of currentDirList" (click)="pathClicked(file)">
            <span [ngClass]="{ 'is-dir': file.isDirectory}">{{ file.fileName }}</span>
            <span *ngIf="this.selectedFile === file.path" class="selected">
              <nb-icon icon="checkmark-circle-outline"></nb-icon>
            </span>
          </nb-list-item>
        </nb-list>
      </nb-card-body>
      <nb-card-footer>
        <button nbButton nbStepperNext [disabled]="selectedFile == null">next</button>
      </nb-card-footer>
    </nb-card>
  `
})
export class WizardStepSelectMovieComponent implements OnDestroy, OnInit {
  currentPath: string = ROOT_PATH;
  currentDirList: FileRecord[] = [];
  selectedFile: string | null = null;

  private ngUnsubscribe$ = new Subject<void>();

  constructor(
    private cdr: ChangeDetectorRef,
    private graph: GraphQLAccessor,
    private spinner: NgxSpinnerService
  ) { }

  ngOnInit(): void {
    this.updateDirList();
  }

  ngOnDestroy(): void {
    this.ngUnsubscribe$.next();
    this.ngUnsubscribe$.complete();
  }

  pathClicked(file: FileRecord): void {
    if (file.isDirectory) {
      this.selectedFile = null;

      if (file.fileName === PARENT_PATH) {
        this.currentPath = file.parent;
      } else {
        this.currentPath = file.path;
      }

      this.updateDirList();

      return;
    }

    if (this.selectedFile === file.path) {
      this.selectedFile = null;
    } else {
      this.selectedFile = file.path;
    }

    this.cdr.detectChanges();
  }

  private updateDirList(): void {
    this.graph
      .Query<FileRecordRequest>(LIST_DIR_QUERY, { path: this.currentPath })
      .pipe(
        startSpinner(this.spinner, 'wizard1'),
        filter((x: FileRecordRequest) => Array.isArray(x.files)),
        map((x: FileRecordRequest) => {
          const files = x.files || [];
          const parentPath = this.currentPath === ROOT_PATH ? '' : this.getParentPath(this.currentPath);
          const parentRecord: FileRecord = {
            isDirectory: true,
            parent: parentPath,
            path: parentPath,
            fileName: PARENT_PATH
          };
          return this.currentPath !== ROOT_PATH ? [parentRecord, ...files] : files;
        }),
        delay(10000),
        endSpinner(this.spinner, 'wizard1'),
        take(1)
      )
      .subscribe((data: FileRecord[]) => {
        this.currentDirList = data;
        this.cdr.detectChanges();
      });
  }

  private getParentPath(path: string): string {
    const parts = path.split(ROOT_PATH);
    parts.pop();
    return parts.join(ROOT_PATH) || ROOT_PATH;
  }
}
