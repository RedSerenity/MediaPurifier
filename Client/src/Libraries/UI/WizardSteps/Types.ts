import { FileRecord } from "@Libraries/GraphQL";

export interface FileRecordRequest {
  files: FileRecord[];
}
