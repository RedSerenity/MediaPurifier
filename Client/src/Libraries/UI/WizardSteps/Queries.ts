import { gql } from "@apollo/client";

export const LIST_DIR_QUERY = gql`
  query ListFiles($path: String!) {
    files(path: $path) {
      fileName
      path
      parent
      isDirectory
    }
  }
`;
