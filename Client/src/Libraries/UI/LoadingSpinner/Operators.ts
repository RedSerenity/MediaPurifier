import { Observable, Subscriber } from "rxjs";
import { NgxSpinnerService } from "ngx-spinner";
 
export const startSpinner = <T>(service: NgxSpinnerService, name?: string): (source: Observable<T>) => Observable<T> =>
  (source: Observable<T>) => new Observable<T>((observer: Subscriber<T>) => {
  // Trigger the loading spinner here
  service.show(name).then();
  console.log('startSpinner', name);

  const subscription = source.subscribe({
    next: (value) => observer.next(value),
    error: (err) => {
      service.hide(name);
      observer.error(err);
    },
    complete: () => {
      // Hide the loading spinner when the observable completes
      service.hide(name);
      observer.complete();
    },
  });

  return () => {
    subscription.unsubscribe();
  };
});

export const endSpinner = <T>(service: NgxSpinnerService, name?: string): (source: Observable<T>) => Observable<T> =>
  (source: Observable<T>) => new Observable<T>((observer: Subscriber<T>) => {
  const subscription = source.subscribe({
    next: (value) => {
      observer.next(value);
      service.hide(name).then();
      console.log('endSpinner', name);
    },
    error: (err) => observer.error(err),
    complete: () => observer.complete(),
  });

  return () => {
    subscription.unsubscribe();
  };
});
