import { ChangeDetectionStrategy, Component, Input, OnChanges, SimpleChanges } from "@angular/core";
import { NgxSpinnerComponent, NgxSpinnerService } from "ngx-spinner";

@Component({
  selector: 'mp-loading-spinner',
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [
    NgxSpinnerComponent
  ],
  template: `
    <ngx-spinner
      [fullScreen]="false"
      type="square-jelly-box"
      size="medium"
      [name]="name"
    ></ngx-spinner>
  `
})
export class LoadingSpinnerComponent implements OnChanges {
  @Input() show: boolean = false;
  @Input() name: string = '';

  constructor(private spinner: NgxSpinnerService) {
  }

  ngOnChanges(changes: SimpleChanges): void {
    const show = changes['show'];
console.debug('ngChanges', changes, show);
    if (show?.currentValue) {
      this.spinner.show(this.name);
    } else {
      this.spinner.hide(this.name);
    }
  }
}
