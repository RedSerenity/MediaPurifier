import { ChangeDetectionStrategy, Component } from "@angular/core";
import { NgForOf } from "@angular/common";
import { NbSelectModule, NbThemeService } from "@nebular/theme";

@Component({
  selector: 'mp-theme-switcher',
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [NbSelectModule, NgForOf],
  template: `
    <nb-select placeholder="Select Theme" [(selected)]="selectedTheme">
      <nb-option *ngFor="let theme of availableThemes" value="{{theme.value}}">{{ theme.name }}</nb-option>
    </nb-select>
  `
})
export class ThemeSwitcherComponent {
  availableThemes: ThemeOption[] = [
    { name: 'Default', value: 'mediapurifier' },
    { name: 'Light', value: 'default' },
    { name: 'Dark', value: 'dark' }
  ];

  private _selectedTheme: string = 'mediapurifier';
  get selectedTheme(): string {
    return this._selectedTheme;
  }

  set selectedTheme(value: string) {
    this._selectedTheme = value;
    this.themes.changeTheme(value);
  }

  constructor(private themes: NbThemeService) {
  }
}

interface ThemeOption {
  name: string;
  value: string;
}
