import gql from 'graphql-tag';
export type Maybe<T> = T | null;
export type InputMaybe<T> = Maybe<T>;
export type Exact<T extends { [key: string]: unknown }> = { [K in keyof T]: T[K] };
export type MakeOptional<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]?: Maybe<T[SubKey]> };
export type MakeMaybe<T, K extends keyof T> = Omit<T, K> & { [SubKey in K]: Maybe<T[SubKey]> };
export type MakeEmpty<T extends { [key: string]: unknown }, K extends keyof T> = { [_ in K]?: never };
export type Incremental<T> = T | { [P in keyof T]?: P extends ' $fragmentName' | '__typename' ? T[P] : never };
/** All built-in and custom scalars, mapped to their actual values */
export type Scalars = {
  ID: { input: string; output: string; }
  String: { input: string; output: string; }
  Boolean: { input: boolean; output: boolean; }
  Int: { input: number; output: number; }
  Float: { input: number; output: number; }
  TimeSpan: { input: any; output: any; }
};

export type AddMovieInput = {
  imDbId?: InputMaybe<Scalars['String']['input']>;
  title: Scalars['String']['input'];
  tvDbId?: InputMaybe<Scalars['String']['input']>;
};

export type AddProfanityInput = {
  maskedWord: Scalars['String']['input'];
  word: Scalars['String']['input'];
};

export type AddSubtitleFileInput = {
  movieId: Scalars['Int']['input'];
  subtitle: Scalars['String']['input'];
};

/** A connection to a list of items. */
export type BadWordsConnection = {
  __typename?: 'BadWordsConnection';
  /** A list of edges. */
  edges?: Maybe<Array<BadWordsEdge>>;
  /** A flattened list of the nodes. */
  nodes?: Maybe<Array<Profanity>>;
  /** Information to aid in pagination. */
  pageInfo: PageInfo;
};

/** An edge in a connection. */
export type BadWordsEdge = {
  __typename?: 'BadWordsEdge';
  /** A cursor for use in pagination. */
  cursor: Scalars['String']['output'];
  /** The item at the end of the edge. */
  node: Profanity;
};

export type BooleanOperationFilterInput = {
  eq?: InputMaybe<Scalars['Boolean']['input']>;
  neq?: InputMaybe<Scalars['Boolean']['input']>;
};

export type FileRecord = {
  __typename?: 'FileRecord';
  fileName?: Maybe<Scalars['String']['output']>;
  isDirectory: Scalars['Boolean']['output'];
  parent: Scalars['String']['output'];
  path: Scalars['String']['output'];
};

export type IntOperationFilterInput = {
  eq?: InputMaybe<Scalars['Int']['input']>;
  gt?: InputMaybe<Scalars['Int']['input']>;
  gte?: InputMaybe<Scalars['Int']['input']>;
  in?: InputMaybe<Array<InputMaybe<Scalars['Int']['input']>>>;
  lt?: InputMaybe<Scalars['Int']['input']>;
  lte?: InputMaybe<Scalars['Int']['input']>;
  neq?: InputMaybe<Scalars['Int']['input']>;
  ngt?: InputMaybe<Scalars['Int']['input']>;
  ngte?: InputMaybe<Scalars['Int']['input']>;
  nin?: InputMaybe<Array<InputMaybe<Scalars['Int']['input']>>>;
  nlt?: InputMaybe<Scalars['Int']['input']>;
  nlte?: InputMaybe<Scalars['Int']['input']>;
};

export type Movie = {
  __typename?: 'Movie';
  id?: Maybe<Scalars['Int']['output']>;
  imDbId?: Maybe<Scalars['String']['output']>;
  title: Scalars['String']['output'];
  tvDbId?: Maybe<Scalars['String']['output']>;
};

export type MovieFilterInput = {
  and?: InputMaybe<Array<MovieFilterInput>>;
  id?: InputMaybe<IntOperationFilterInput>;
  imDbId?: InputMaybe<StringOperationFilterInput>;
  or?: InputMaybe<Array<MovieFilterInput>>;
  title?: InputMaybe<StringOperationFilterInput>;
  tvDbId?: InputMaybe<StringOperationFilterInput>;
};

export type MovieSortInput = {
  id?: InputMaybe<SortEnumType>;
  imDbId?: InputMaybe<SortEnumType>;
  title?: InputMaybe<SortEnumType>;
  tvDbId?: InputMaybe<SortEnumType>;
};

/** A connection to a list of items. */
export type MoviesConnection = {
  __typename?: 'MoviesConnection';
  /** A list of edges. */
  edges?: Maybe<Array<MoviesEdge>>;
  /** A flattened list of the nodes. */
  nodes?: Maybe<Array<Movie>>;
  /** Information to aid in pagination. */
  pageInfo: PageInfo;
};

/** An edge in a connection. */
export type MoviesEdge = {
  __typename?: 'MoviesEdge';
  /** A cursor for use in pagination. */
  cursor: Scalars['String']['output'];
  /** The item at the end of the edge. */
  node: Movie;
};

export type Mutations = {
  __typename?: 'Mutations';
  addMovie: Movie;
  addProfanity?: Maybe<Profanity>;
  addSubtitle: Subtitle;
  addSubtitleFile: SubtitleFile;
  deleteMovie: Scalars['Boolean']['output'];
  deleteProfanity: Scalars['Boolean']['output'];
  deleteSubtitleFile: Scalars['Boolean']['output'];
  parseSubtitleFile: Scalars['Boolean']['output'];
  updateMovie: Movie;
  updateSubtitleFile: SubtitleFile;
};


export type MutationsAddMovieArgs = {
  input: AddMovieInput;
};


export type MutationsAddProfanityArgs = {
  input: AddProfanityInput;
};


export type MutationsAddSubtitleFileArgs = {
  fileInput: AddSubtitleFileInput;
};


export type MutationsDeleteMovieArgs = {
  movieId: Scalars['ID']['input'];
};


export type MutationsDeleteProfanityArgs = {
  profanityId: Scalars['ID']['input'];
};


export type MutationsDeleteSubtitleFileArgs = {
  id: Scalars['ID']['input'];
};


export type MutationsParseSubtitleFileArgs = {
  id: Scalars['ID']['input'];
};


export type MutationsUpdateMovieArgs = {
  input: UpdateMovieInput;
};


export type MutationsUpdateSubtitleFileArgs = {
  fileInput: UpdateSubtitleFileInput;
};

/** Information about pagination in a connection. */
export type PageInfo = {
  __typename?: 'PageInfo';
  /** When paginating forwards, the cursor to continue. */
  endCursor?: Maybe<Scalars['String']['output']>;
  /** Indicates whether more edges exist following the set defined by the clients arguments. */
  hasNextPage: Scalars['Boolean']['output'];
  /** Indicates whether more edges exist prior the set defined by the clients arguments. */
  hasPreviousPage: Scalars['Boolean']['output'];
  /** When paginating backwards, the cursor to continue. */
  startCursor?: Maybe<Scalars['String']['output']>;
};

export type Profanity = {
  __typename?: 'Profanity';
  id?: Maybe<Scalars['Int']['output']>;
  masked?: Maybe<Scalars['String']['output']>;
  maskedWord: Scalars['String']['output'];
  word: Scalars['String']['output'];
};

export type ProfanityFilterInput = {
  and?: InputMaybe<Array<ProfanityFilterInput>>;
  id?: InputMaybe<IntOperationFilterInput>;
  masked?: InputMaybe<StringOperationFilterInput>;
  or?: InputMaybe<Array<ProfanityFilterInput>>;
  word?: InputMaybe<StringOperationFilterInput>;
};

export type ProfanitySortInput = {
  id?: InputMaybe<SortEnumType>;
  masked?: InputMaybe<SortEnumType>;
  word?: InputMaybe<SortEnumType>;
};

export type Queries = {
  __typename?: 'Queries';
  badWord?: Maybe<Profanity>;
  badWords?: Maybe<BadWordsConnection>;
  files: Array<FileRecord>;
  movie?: Maybe<Movie>;
  movies?: Maybe<MoviesConnection>;
  subtitle?: Maybe<Subtitle>;
  subtitleFile?: Maybe<SubtitleFile>;
  subtitleFiles?: Maybe<SubtitleFilesConnection>;
  subtitles?: Maybe<SubtitlesConnection>;
};


export type QueriesBadWordArgs = {
  where?: InputMaybe<ProfanityFilterInput>;
};


export type QueriesBadWordsArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  before?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  order?: InputMaybe<Array<ProfanitySortInput>>;
  where?: InputMaybe<ProfanityFilterInput>;
};


export type QueriesFilesArgs = {
  filter?: InputMaybe<Scalars['String']['input']>;
  path: Scalars['String']['input'];
};


export type QueriesMovieArgs = {
  where?: InputMaybe<MovieFilterInput>;
};


export type QueriesMoviesArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  before?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  order?: InputMaybe<Array<MovieSortInput>>;
  where?: InputMaybe<MovieFilterInput>;
};


export type QueriesSubtitleArgs = {
  where?: InputMaybe<SubtitleFilterInput>;
};


export type QueriesSubtitleFileArgs = {
  where?: InputMaybe<SubtitleFileFilterInput>;
};


export type QueriesSubtitleFilesArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  before?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  order?: InputMaybe<Array<SubtitleFileSortInput>>;
  where?: InputMaybe<SubtitleFileFilterInput>;
};


export type QueriesSubtitlesArgs = {
  after?: InputMaybe<Scalars['String']['input']>;
  before?: InputMaybe<Scalars['String']['input']>;
  first?: InputMaybe<Scalars['Int']['input']>;
  last?: InputMaybe<Scalars['Int']['input']>;
  order?: InputMaybe<Array<SubtitleSortInput>>;
  where?: InputMaybe<SubtitleFilterInput>;
};

export enum SortEnumType {
  Asc = 'ASC',
  Desc = 'DESC'
}

export type StringOperationFilterInput = {
  and?: InputMaybe<Array<StringOperationFilterInput>>;
  contains?: InputMaybe<Scalars['String']['input']>;
  endsWith?: InputMaybe<Scalars['String']['input']>;
  eq?: InputMaybe<Scalars['String']['input']>;
  in?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  ncontains?: InputMaybe<Scalars['String']['input']>;
  nendsWith?: InputMaybe<Scalars['String']['input']>;
  neq?: InputMaybe<Scalars['String']['input']>;
  nin?: InputMaybe<Array<InputMaybe<Scalars['String']['input']>>>;
  nstartsWith?: InputMaybe<Scalars['String']['input']>;
  or?: InputMaybe<Array<StringOperationFilterInput>>;
  startsWith?: InputMaybe<Scalars['String']['input']>;
};

export type Subtitle = {
  __typename?: 'Subtitle';
  blockId: Scalars['Int']['output'];
  end: Scalars['TimeSpan']['output'];
  hasProfanity: Scalars['Boolean']['output'];
  id?: Maybe<Scalars['Int']['output']>;
  movieId?: Maybe<Scalars['Int']['output']>;
  start: Scalars['TimeSpan']['output'];
  subtitleFileId?: Maybe<Scalars['Int']['output']>;
  text: Scalars['String']['output'];
};

export type SubtitleFile = {
  __typename?: 'SubtitleFile';
  file: Scalars['String']['output'];
  id?: Maybe<Scalars['Int']['output']>;
  movieId?: Maybe<Scalars['Int']['output']>;
};

export type SubtitleFileFilterInput = {
  and?: InputMaybe<Array<SubtitleFileFilterInput>>;
  file?: InputMaybe<StringOperationFilterInput>;
  id?: InputMaybe<IntOperationFilterInput>;
  movieId?: InputMaybe<IntOperationFilterInput>;
  or?: InputMaybe<Array<SubtitleFileFilterInput>>;
};

export type SubtitleFileSortInput = {
  file?: InputMaybe<SortEnumType>;
  id?: InputMaybe<SortEnumType>;
  movieId?: InputMaybe<SortEnumType>;
};

/** A connection to a list of items. */
export type SubtitleFilesConnection = {
  __typename?: 'SubtitleFilesConnection';
  /** A list of edges. */
  edges?: Maybe<Array<SubtitleFilesEdge>>;
  /** A flattened list of the nodes. */
  nodes?: Maybe<Array<SubtitleFile>>;
  /** Information to aid in pagination. */
  pageInfo: PageInfo;
};

/** An edge in a connection. */
export type SubtitleFilesEdge = {
  __typename?: 'SubtitleFilesEdge';
  /** A cursor for use in pagination. */
  cursor: Scalars['String']['output'];
  /** The item at the end of the edge. */
  node: SubtitleFile;
};

export type SubtitleFilterInput = {
  and?: InputMaybe<Array<SubtitleFilterInput>>;
  blockId?: InputMaybe<IntOperationFilterInput>;
  end?: InputMaybe<TimeSpanOperationFilterInput>;
  hasProfanity?: InputMaybe<BooleanOperationFilterInput>;
  id?: InputMaybe<IntOperationFilterInput>;
  movieId?: InputMaybe<IntOperationFilterInput>;
  or?: InputMaybe<Array<SubtitleFilterInput>>;
  start?: InputMaybe<TimeSpanOperationFilterInput>;
  subtitleFileId?: InputMaybe<IntOperationFilterInput>;
  text?: InputMaybe<StringOperationFilterInput>;
};

export type SubtitleSortInput = {
  blockId?: InputMaybe<SortEnumType>;
  end?: InputMaybe<SortEnumType>;
  hasProfanity?: InputMaybe<SortEnumType>;
  id?: InputMaybe<SortEnumType>;
  movieId?: InputMaybe<SortEnumType>;
  start?: InputMaybe<SortEnumType>;
  subtitleFileId?: InputMaybe<SortEnumType>;
  text?: InputMaybe<SortEnumType>;
};

/** A connection to a list of items. */
export type SubtitlesConnection = {
  __typename?: 'SubtitlesConnection';
  /** A list of edges. */
  edges?: Maybe<Array<SubtitlesEdge>>;
  /** A flattened list of the nodes. */
  nodes?: Maybe<Array<Subtitle>>;
  /** Information to aid in pagination. */
  pageInfo: PageInfo;
};

/** An edge in a connection. */
export type SubtitlesEdge = {
  __typename?: 'SubtitlesEdge';
  /** A cursor for use in pagination. */
  cursor: Scalars['String']['output'];
  /** The item at the end of the edge. */
  node: Subtitle;
};

export type TimeSpanOperationFilterInput = {
  eq?: InputMaybe<Scalars['TimeSpan']['input']>;
  gt?: InputMaybe<Scalars['TimeSpan']['input']>;
  gte?: InputMaybe<Scalars['TimeSpan']['input']>;
  in?: InputMaybe<Array<InputMaybe<Scalars['TimeSpan']['input']>>>;
  lt?: InputMaybe<Scalars['TimeSpan']['input']>;
  lte?: InputMaybe<Scalars['TimeSpan']['input']>;
  neq?: InputMaybe<Scalars['TimeSpan']['input']>;
  ngt?: InputMaybe<Scalars['TimeSpan']['input']>;
  ngte?: InputMaybe<Scalars['TimeSpan']['input']>;
  nin?: InputMaybe<Array<InputMaybe<Scalars['TimeSpan']['input']>>>;
  nlt?: InputMaybe<Scalars['TimeSpan']['input']>;
  nlte?: InputMaybe<Scalars['TimeSpan']['input']>;
};

export type UpdateMovieInput = {
  id: Scalars['Int']['input'];
  imDbId?: InputMaybe<Scalars['String']['input']>;
  title: Scalars['String']['input'];
  tvDbId?: InputMaybe<Scalars['String']['input']>;
};

export type UpdateSubtitleFileInput = {
  id: Scalars['Int']['input'];
  subtitle: Scalars['String']['input'];
};
