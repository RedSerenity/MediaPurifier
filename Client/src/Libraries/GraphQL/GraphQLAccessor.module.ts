import { NgModule } from "@angular/core";
import { APOLLO_OPTIONS, ApolloModule } from "apollo-angular";
import { HttpClientModule } from "@angular/common/http";
import { HttpLink } from "apollo-angular/http";
import { InMemoryCache } from "@apollo/client/cache";

import { GraphQLAccessor } from "./GraphQLAccessor.service";

@NgModule({
  imports: [ApolloModule, HttpClientModule],
  providers: [
    GraphQLAccessor,
    {
      provide: APOLLO_OPTIONS,
      useFactory(httpLink: HttpLink) {
        return {
          connectToDevTools: true,
          cache: new InMemoryCache(),
          link: httpLink.create({
            uri: 'http://localhost:5284/graphql'
          })
        };
      },
      deps: [HttpLink]
    }
  ]
})
export class GraphQLAccessorModule {
}
