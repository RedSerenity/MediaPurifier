import { Injectable } from '@angular/core';
import { Apollo } from 'apollo-angular';
import { map, Observable } from 'rxjs';
import { EmptyObject, MutationResult } from 'apollo-angular/types';
import { ApolloQueryResult, OperationVariables, TypedDocumentNode } from '@apollo/client/core';

@Injectable({ providedIn: 'root' })
export class GraphQLAccessor {
  constructor(private readonly apollo: Apollo) {
  }

  Query<T, V extends OperationVariables = EmptyObject>(gql: TypedDocumentNode<T, V>, variables: V): Observable<T> {
    return this.apollo
      .query<T, V>({ query: gql, variables })
      .pipe(
        map((result: ApolloQueryResult<T>) => result.data)
      );
  }

  Mutate<T, V extends OperationVariables = EmptyObject>(gql: TypedDocumentNode<T, V>, variables: V): Observable<T | null | undefined> {
    return this.apollo
      .mutate<T, V>({ mutation: gql, variables })
      .pipe(
        map((result: MutationResult<T>) => result.data)
      );
  }
}
