import { Routes } from "@angular/router";

import { HomePage } from "./Home.page";
import { MoviesPage } from "./Movies.page";

export const PAGE_ROUTES: Routes = [
  {
    path: '',
    component: HomePage
  },
  {
    path: 'movies',
    component: MoviesPage
  }
];
