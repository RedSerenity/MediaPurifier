import { ChangeDetectionStrategy, Component, OnDestroy, OnInit } from "@angular/core";
import { NbButtonModule, NbCardModule, NbStepperModule } from "@nebular/theme";
import { Subject } from "rxjs";

import {
  GraphQLAccessorModule
} from "@Libraries/GraphQL";
import { WizardStepSelectMovieComponent } from "@Libraries/UI";

@Component({
  selector: 'mp-home-page',
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [
    NbStepperModule,
    NbButtonModule,
    NbCardModule,
    GraphQLAccessorModule,
    WizardStepSelectMovieComponent
  ],
  template: `
    <nb-stepper orientation="horizontal">
      <nb-step [label]="labelOne">
        <ng-template #labelOne>Select Movie File</ng-template>
        <mp-wizard-step-select-movie></mp-wizard-step-select-movie>
      </nb-step>
      <nb-step [label]="labelTwo">
        <ng-template #labelTwo>Second step</ng-template>
        <nb-card>
          <nb-card-header>Select Movie</nb-card-header>
          <nb-card-body>
            a
          </nb-card-body>
          <nb-card-footer>
            <button nbButton nbStepperPrevious>prev</button>
            <button nbButton nbStepperNext>next</button>
          </nb-card-footer>
        </nb-card>
      </nb-step>
      <nb-step [label]="labelThree">
        <ng-template #labelThree>Third step</ng-template>
        <nb-card>
          <nb-card-header>Select Movie</nb-card-header>
          <nb-card-body>
            a
          </nb-card-body>
          <nb-card-footer>
            <button nbButton nbStepperPrevious>prev</button>
            <button nbButton nbStepperNext>next</button>
          </nb-card-footer>
        </nb-card>
      </nb-step>
      <nb-step [label]="labelFour">
        <ng-template #labelFour>Forth step</ng-template>
        <nb-card>
          <nb-card-header>Select Movie</nb-card-header>
          <nb-card-body>
            a
          </nb-card-body>
          <nb-card-footer>
            <button nbButton nbStepperPrevious>prev</button>
            <button nbButton disabled nbStepperNext>next</button>
          </nb-card-footer>
        </nb-card>
      </nb-step>
    </nb-stepper>
  `,
  styleUrl: './Home.page.scss'
})
export class HomePage implements OnDestroy, OnInit {
  private ngUnsubscribe$ = new Subject<void>();

  constructor() {}

  ngOnInit(): void {
    //
  }

  ngOnDestroy(): void {
    this.ngUnsubscribe$.next();
    this.ngUnsubscribe$.complete();
  }
}

