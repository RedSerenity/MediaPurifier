import { ChangeDetectionStrategy, Component } from "@angular/core";

@Component({
  selector: 'mp-movie-page',
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [],
  template: `
    Movies Page
  `
})
export class MoviesPage {

}
