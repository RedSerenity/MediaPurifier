import { bootstrapApplication } from '@angular/platform-browser';

import { ApplicationConfig, ApplicationComponent } from '@App';

bootstrapApplication(ApplicationComponent, ApplicationConfig)
  .catch((err) => console.error(err));
