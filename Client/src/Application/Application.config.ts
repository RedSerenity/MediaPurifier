import {
  ApplicationConfig as AngularApplicationConfig,
  importProvidersFrom
} from '@angular/core';
import { provideAnimations } from "@angular/platform-browser/animations";
import { provideHttpClient } from "@angular/common/http";
import { provideRouter } from '@angular/router';
import { NbSidebarModule, NbThemeModule } from '@nebular/theme';
import { NbEvaIconsModule } from "@nebular/eva-icons";

import { APP_ROUTES } from './Application.routes';
import { NgxSpinnerModule } from "ngx-spinner";

export const ApplicationConfig: AngularApplicationConfig = {
  providers: [
    provideAnimations(),
    provideRouter(APP_ROUTES),
    provideHttpClient(),
    importProvidersFrom(
      NbThemeModule.forRoot({ name: 'mediapurifier' }),
      NbSidebarModule.forRoot(),
      NbEvaIconsModule,
      NgxSpinnerModule.forRoot({ type: 'square-jelly-box' })
    )
  ]
};
