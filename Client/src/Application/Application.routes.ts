import { Routes } from '@angular/router';

import { HomePage } from "@Pages";

export const APP_ROUTES: Routes = [
  {
    path: '',
    component: HomePage
  },
  {
    path: 'page',
    loadChildren: () => import('../Pages/Pages.routes')
      .then(m => m.PAGE_ROUTES)
  }
];
