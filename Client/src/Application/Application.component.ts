import { ChangeDetectionStrategy, Component } from '@angular/core';
import { RouterOutlet } from '@angular/router';
import {
  NbButtonModule,
  NbIconModule,
  NbLayoutModule,
  NbSidebarModule,
  NbSidebarService
} from '@nebular/theme';

import { ThemeSwitcherComponent } from "@Libraries/UI";

@Component({
  selector: 'mp-root',
  standalone: true,
  changeDetection: ChangeDetectionStrategy.OnPush,
  imports: [
    RouterOutlet,
    NbLayoutModule,
    NbSidebarModule,
    NbButtonModule,
    NbIconModule,
    ThemeSwitcherComponent
  ],
  template: `
    <nb-layout>
      <nb-layout-header subheader>
        <div class="layout-header">
          <button nbButton (click)="toggle()">
            <nb-icon icon="menu-outline"></nb-icon>
          </button>
          <mp-theme-switcher></mp-theme-switcher>
        </div>
      </nb-layout-header>
      <nb-sidebar></nb-sidebar>
      <nb-layout-column>
        <router-outlet></router-outlet>
      </nb-layout-column>
    </nb-layout>
  `,
  styleUrl: './Application.component.scss',
})
export class ApplicationComponent {
  constructor(private sidebarService: NbSidebarService) {
  }

  toggle() {
    this.sidebarService.toggle(true);
  }

}
