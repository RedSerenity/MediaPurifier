
import type { CodegenConfig } from '@graphql-codegen/cli';

const config: CodegenConfig = {
  overwrite: true,
  schema: "http://app.mp.dev:5284/graphql",
  generates: {
    "src/Libraries/GraphQL/types.ts": {
      plugins: ["typescript", "typescript-document-nodes"]
    },
    "./graphql.schema.json": {
      plugins: ["introspection"]
    }
  }
};

export default config;
