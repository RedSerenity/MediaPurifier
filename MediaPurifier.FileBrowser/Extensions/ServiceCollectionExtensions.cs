using MediaPurifier.FileBrowser.Abstractions;
using MediaPurifier.FileBrowser.Services;
using Microsoft.Extensions.DependencyInjection;

namespace MediaPurifier.FileBrowser.Extensions;

public static class ServiceCollectionExtensions {
	public static IServiceCollection AddFileBrowser(this IServiceCollection services) =>
		services
			.AddScoped<IFileService, FileService>();
}
