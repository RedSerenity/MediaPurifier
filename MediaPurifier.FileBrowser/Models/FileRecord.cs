namespace MediaPurifier.FileBrowser.Models;

public record FileRecord {
	public required string Parent { get; init; }
	public required string Path { get; init; }
	public string? FileName { get; init; }
	public required bool IsDirectory { get; init; }
};
