using Microsoft.Extensions.Logging;

namespace MediaPurifier.FileBrowser;

public static class LogEventIds {
	public static readonly EventId FileBrowser = 5120;
}
