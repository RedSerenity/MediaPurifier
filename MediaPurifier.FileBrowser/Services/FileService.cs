using System.Text.RegularExpressions;
using MediaPurifier.FileBrowser.Abstractions;
using MediaPurifier.FileBrowser.Models;
using Microsoft.Extensions.Logging;

namespace MediaPurifier.FileBrowser.Services;

public class FileService(ILogger<FileService> logger) : IFileService {
	public async Task<List<FileRecord>> ListFilesFilterAsync(string path, string filter) {
		return await ListFilesAsync(path, f => LikeOperator(f.Name, filter));
	}

	public async Task<List<FileRecord>> ListFilesAsync(string path, Func<FileSystemInfo, bool>? filter = null) {
		try {
			if (String.IsNullOrEmpty(path)) {
				throw new ArgumentOutOfRangeException(nameof(path));
			}

			var dirInfo = new DirectoryInfo(path);

			var fileSystemInfos = dirInfo.EnumerateFileSystemInfos();

			if (filter is not null) {
				fileSystemInfos = fileSystemInfos.Where(filter);
			}

			var fileRecords = fileSystemInfos
				.Select(
					info => new FileRecord {
						Parent = GetParentPath(info),
						Path = info.FullName,
						FileName = info.Name,
						IsDirectory = (info.Attributes & FileAttributes.Directory) == FileAttributes.Directory
					}
				)
				.ToList();

			return await Task.FromResult(fileRecords);
		} catch (Exception ex) {
			logger.LogError(LogEventIds.FileBrowser, ex, "Unable to list path");

			return new List<FileRecord>();
		}
	}

	private bool LikeOperator(string stringToMatch, string pattern) {
		// Replace * with .* and make the search case-insensitive
		var regexPattern = $"^{Regex.Escape(pattern).Replace("\\*", ".*")}$";

		return Regex.IsMatch(stringToMatch, regexPattern, RegexOptions.IgnoreCase | RegexOptions.Singleline);
	}

	private string GetParentPath(FileSystemInfo info) {
		var path = Path.GetDirectoryName(info.FullName);

		return !String.IsNullOrEmpty(path) ? path : "/";
	}
}
