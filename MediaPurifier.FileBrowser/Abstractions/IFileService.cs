using MediaPurifier.FileBrowser.Models;

namespace MediaPurifier.FileBrowser.Abstractions;

public interface IFileService {
	Task<List<FileRecord>> ListFilesAsync(string path, Func<FileSystemInfo, bool>? filter = null);
	Task<List<FileRecord>> ListFilesFilterAsync(string path, string filter);
}
