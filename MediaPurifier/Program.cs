using MediaPurifier.Data;
using MediaPurifier.Data.Extensions;
using MediaPurifier.FileBrowser.Extensions;
using MediaPurifier.GraphQL.Extensions;
using MediaPurifier.Movies.Extensions;
using MediaPurifier.ProfanityFilter.Extensions;
using MediaPurifier.Subtitles.Extensions;
using Serilog;
using Serilog.Sinks.Console.LogThemes;

Log.Logger = new LoggerConfiguration()
	.WriteTo.Console()
	.CreateBootstrapLogger();

try {
	Log.Information("Starting MediaPurifier!");

	var builder = WebApplication.CreateBuilder(args);

	builder.Services
		.AddCors(
			o => {
				o.AddDefaultPolicy(b => b.AllowAnyOrigin().AllowAnyHeader().AllowAnyHeader());
			}
		);
	
	// Add services to the container.
	builder.Services
		.AddSerilog(
			(services, lc) =>
				lc.ReadFrom.Configuration(builder.Configuration)
					.ReadFrom.Services(services)
					.Enrich.FromLogContext()
					.WriteTo.Console(
						outputTemplate:
						"[{Timestamp:HH:mm:ss} {Level:u3}] [{SourceContext}] {Message:lj}{NewLine}{Exception}",
						theme: LogThemes.Code
					)
					.WriteTo.File(Path.Combine(Path.GetTempPath(), "media-purifier.log"))
		)
		.AddFileBrowser()
		.AddMovies()
		.AddProfanityFilter()
		.AddSubtitles()
		.AddDatabase(builder.Configuration)
		.AddMpGraphQL<MediaPurifierContext>();

	builder.Services
		.AddControllers();

	builder.Services
		.AddEndpointsApiExplorer()
		.AddSwaggerGen();

	var app = builder.Build();

	app.UseCors();
	
	// Configure the HTTP request pipeline.
	if (app.Environment.IsDevelopment())
		app
			.UseSwagger()
			.UseSwaggerUI();

	app
		.UseSerilogRequestLogging()
		.UseHttpsRedirection()
		.UseAuthorization();

	app.MapMpGraphQL();
	app.MapControllers();

	await app.RunAsync();
} catch (Exception ex) {
	Log.Fatal(ex, "Application terminated unexpectedly");
} finally {
	Log.CloseAndFlush();
}
