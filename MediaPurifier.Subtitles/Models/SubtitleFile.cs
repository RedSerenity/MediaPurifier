namespace MediaPurifier.Subtitles.Models;

public record SubtitleFile {
	public int? Id { get; init; }
	public required string File { get; init; }
	public int? MovieId { get; init; }
}
