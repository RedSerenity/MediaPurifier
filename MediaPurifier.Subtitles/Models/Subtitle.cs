namespace MediaPurifier.Subtitles.Models;

public record Subtitle {
	public int? Id { get; init; }
	public int BlockId { get; init; }
	public required string Text { get; init; }
	public required TimeSpan Start { get; init; }
	public required TimeSpan End { get; init; }
	public bool HasProfanity { get; init; }

	public int? SubtitleFileId { get; init; }
	public int? MovieId { get; init; }
}
