using MediaPurifier.Subtitles.Models;

namespace MediaPurifier.Subtitles.Abstractions;

public interface ISubtitleFileService {
	IQueryable<SubtitleFile> QuerySubtitleFile();
	Task<SubtitleFile> AddAsync(int movieId, string file, CancellationToken cancellationToken = default);
	Task<SubtitleFile> GetAsync(int id, CancellationToken cancellationToken = default);
	Task<string[]> GetAsArrayAsync(int id, CancellationToken cancellationToken = default);
	Task<List<SubtitleFile>> GetByMovieIdAsync(int movieId, CancellationToken cancellationToken = default);
	Task<List<SubtitleFile>> ListAsync(CancellationToken cancellationToken = default);
	Task<SubtitleFile> UpdateAsync(SubtitleFile subtitleFile, CancellationToken cancellationToken = default);
	Task<bool> DeleteAsync(int id, CancellationToken cancellationToken = default);
	Task<bool> ParseFileAsync(int id, CancellationToken cancellationToken = default);
}
