﻿namespace MediaPurifier.Subtitles.Abstractions;

public interface ISubtitleTimeFormat {
	public int StartTimeOffset();
	public int StartTimeLength();
	public int EndTimeOffset();
	public int EndTimeLength();
	public string TimeStringFormat();
}
