using MediaPurifier.Subtitles.Models;

namespace MediaPurifier.Subtitles.Abstractions;

public interface ISubtitleService {
	IQueryable<Subtitle> QuerySubtitles();

	Task<Subtitle> AddAsync(
		int blockId, TimeSpan start, TimeSpan end, string text, int movieId,
		CancellationToken cancellationToken = default
	);

	Task<Subtitle> GetAsync(int id, CancellationToken cancellationToken = default);
	Task<List<Subtitle>> GetByMovieAsync(int id, CancellationToken cancellationToken = default);
	Task<List<Subtitle>> GetBySubtitleFileAsync(int id, CancellationToken cancellationToken = default);
	Task<Subtitle> UpdateAsync(Subtitle subtitle, CancellationToken cancellationToken = default);
	Task DeleteAsync(int id, CancellationToken cancellationToken = default);
}
