﻿using MediaPurifier.Subtitles.Abstractions;

namespace MediaPurifier.Subtitles;

public class SubtitleTimeFormat : ISubtitleTimeFormat {
	public int StartTimeOffset() { return 0; }

	public int StartTimeLength() { return 12; }

	public int EndTimeOffset() { return 17; }

	public int EndTimeLength() { return 12; }

	public string TimeStringFormat() { return @"hh\:mm\:ss\,fff"; }
}
