using MediaPurifier.Subtitles.Abstractions;
using MediaPurifier.Subtitles.Services;
using Microsoft.Extensions.DependencyInjection;

namespace MediaPurifier.Subtitles.Extensions;

public static class ServiceCollectionExtensions {
	public static IServiceCollection AddSubtitles(this IServiceCollection services) =>
		services
			.AddScoped<ISubtitleFileService, SubtitleFileService>()
			.AddScoped<ISubtitleService, SubtitleService>()
			.AddScoped<SubtitleParser>();
}
