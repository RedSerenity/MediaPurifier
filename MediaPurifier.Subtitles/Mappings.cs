using MediaPurifier.Data.Entities;
using MediaPurifier.Subtitles.Models;

namespace MediaPurifier.Subtitles;

public static class Mappings {
	public static SubtitleFileEntity ToEntity(this SubtitleFile subtitleFile) {
		return new SubtitleFileEntity {
			SubtitleFileId = subtitleFile.Id ?? 0,
			SubtitleFile = subtitleFile.File,
			MovieId = subtitleFile.MovieId ?? 0
		};
	}

	public static SubtitleFile ToModel(this SubtitleFileEntity subtitleFileEntity) {
		return new SubtitleFile {
			Id = subtitleFileEntity.SubtitleFileId,
			File = subtitleFileEntity.SubtitleFile,
			MovieId = subtitleFileEntity.MovieId
		};
	}

	public static SubtitleEntity ToEntity(this Subtitle subtitle) {
		return new SubtitleEntity {
			SubtitleId = subtitle.Id ?? 0,
			BlockId = subtitle.BlockId,
			StartTime = subtitle.Start,
			EndTime = subtitle.End,
			Text = subtitle.Text,
			HasProfanity = subtitle.HasProfanity,
			MovieId = subtitle.MovieId ?? 0,
			SubtitleFileId = subtitle.SubtitleFileId ?? 0
		};
	}

	public static Subtitle ToModel(this SubtitleEntity subtitleEntity) {
		return new Subtitle {
			Id = subtitleEntity.SubtitleId,
			BlockId = subtitleEntity.BlockId,
			Start = subtitleEntity.StartTime,
			End = subtitleEntity.EndTime,
			Text = subtitleEntity.Text,
			HasProfanity = subtitleEntity.HasProfanity,
			MovieId = subtitleEntity.MovieId,
			SubtitleFileId = subtitleEntity.SubtitleFileId
		};
	}
}
