using System.Text;
using MediaPurifier.Subtitles.Models;
using Microsoft.Extensions.Logging;

namespace MediaPurifier.Subtitles;

public class SubtitleParser(ILogger<SubtitleParser> logger) {
	public List<Subtitle> LoadAndParseFile(string filePath, SubtitleTimeFormat? subtitleTimeFormat = null) {
		var lines = LoadSubtitleFile(filePath);

		return ParseSubtitleList(lines, subtitleTimeFormat);
	}

	public string[] LoadSubtitleFile(string filePath) {
		logger.LogInformation("Loading subtitle file {File}", filePath);

		try {
			var fileContents = File.ReadAllLines(filePath, Encoding.UTF8);
			var lastIndex = fileContents.Length;

			if (fileContents[lastIndex - 1] != "") {
				var newFileContents = new string[lastIndex + 1];
				Array.Copy(fileContents, newFileContents, lastIndex);
				newFileContents[lastIndex] = "";

				fileContents = newFileContents;
			}

			logger.LogInformation("Loaded {File} (Lines: {Lines})", filePath, fileContents.Length);

			return fileContents;
		} catch (Exception ex) {
			logger.LogError(ex, "Error loading subtitles from {File}", filePath);

			return [ ];
		}
	}

	public List<Subtitle> ParseSubtitleList(string[] subtitleLines, SubtitleTimeFormat? subtitleTimeFormat = null) {
		subtitleTimeFormat ??= new SubtitleTimeFormat();
		var subtitleBlockList = new List<Subtitle>();

		var srtLines = new List<string>();

		foreach (var subtitle in subtitleLines)
			if (subtitle == "") {
				if (srtLines.Count > 2) {
					var sb = ParseSubtitleBlock(srtLines, subtitleTimeFormat);
					subtitleBlockList.Add(sb);
				}

				srtLines.Clear();
			} else {
				srtLines.Add(subtitle);
			}

		logger.LogInformation("Parsed subtitles into {Count} blocks", srtLines.Count);

		return subtitleBlockList;
	}

	public Subtitle ParseSubtitleBlock(List<string> srtLines, SubtitleTimeFormat subtitleTimeFormat) {
		var blockId = ExtractBlockId(srtLines[0]);
		var startTime = ExtractStartTime(srtLines[1], subtitleTimeFormat);
		var endTime = ExtractEndTime(srtLines[1], subtitleTimeFormat);
		var inlineTextList = srtLines.Skip(2).ToList();

		return new Subtitle {
			Id = null,
			BlockId = blockId,
			Text = string.Join("\n", inlineTextList),
			Start = startTime,
			End = endTime,
			HasProfanity = false,
			SubtitleFileId = null,
			MovieId = null
		};
	}

	private int ExtractBlockId(string orderLine) { return Convert.ToInt32(orderLine.Split('.')[0]); }

	private TimeSpan ExtractStartTime(string timeLine, SubtitleTimeFormat subtitleTimeFormat) {
		var timeString = timeLine.Substring(
			subtitleTimeFormat.StartTimeOffset(),
			subtitleTimeFormat.StartTimeLength()
		);

		return TimeSpan.ParseExact(timeString, subtitleTimeFormat.TimeStringFormat(), null);
	}

	private TimeSpan ExtractEndTime(string timeLine, SubtitleTimeFormat subtitleTimeFormat) {
		var timeString = timeLine.Substring(
			subtitleTimeFormat.EndTimeOffset(),
			subtitleTimeFormat.EndTimeLength()
		);

		return TimeSpan.ParseExact(timeString, subtitleTimeFormat.TimeStringFormat(), null);
	}
}
