using MediaPurifier.Data;
using MediaPurifier.Data.Entities;
using MediaPurifier.Data.Exceptions;
using MediaPurifier.Subtitles.Abstractions;
using MediaPurifier.Subtitles.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace MediaPurifier.Subtitles.Services;

public class SubtitleService(ILogger<SubtitleService> logger, MediaPurifierContext dbContext) : ISubtitleService {
	public IQueryable<Subtitle> QuerySubtitles() {
		return dbContext.Subtitles
			.AsNoTracking()
			.Select(x => x.ToModel());
	}

	public async Task<Subtitle> AddAsync(
		int blockId, TimeSpan start, TimeSpan end, string text, int movieId,
		CancellationToken cancellationToken = default
	) {
		try {
			cancellationToken.ThrowIfCancellationRequested();

			var entity = new SubtitleEntity {
				BlockId = blockId,
				StartTime = start,
				EndTime = end,
				Text = text,
				MovieId = movieId
			};

			await dbContext.Subtitles.AddAsync(entity, cancellationToken);
			await dbContext.SaveChangesAsync(cancellationToken);

			return entity.ToModel();
		} catch (OperationCanceledException) {
			logger.LogWarning(LogEventIds.Subtitles, "Operation was cancelled");

			throw;
		}
	}

	public async Task<Subtitle> GetAsync(int id, CancellationToken cancellationToken = default) {
		try {
			cancellationToken.ThrowIfCancellationRequested();

			var entity = await dbContext.Subtitles.AsNoTracking()
				.FirstOrDefaultAsync(x => x.SubtitleId == id, cancellationToken);

			if (entity == null) {
				logger.LogWarning(LogEventIds.Subtitles, "Subtitle with id {Id} not found", id);

				throw new EntityNotFoundException<SubtitleEntity>(id);
			}

			return entity.ToModel();
		} catch (OperationCanceledException) {
			logger.LogWarning(LogEventIds.Subtitles, "Operation was cancelled");

			throw;
		}
	}

	public async Task<List<Subtitle>> GetByMovieAsync(int id, CancellationToken cancellationToken = default) {
		try {
			cancellationToken.ThrowIfCancellationRequested();

			var entities = await dbContext.Subtitles.AsNoTracking()
				.Where(x => x.MovieId == id)
				.ToListAsync(cancellationToken);

			return entities.Select(x => x.ToModel()).ToList();
		} catch (OperationCanceledException) {
			logger.LogWarning(LogEventIds.Subtitles, "Operation was cancelled");

			throw;
		}
	}

	public async Task<List<Subtitle>> GetBySubtitleFileAsync(
		int id,
		CancellationToken cancellationToken = default
	) {
		try {
			cancellationToken.ThrowIfCancellationRequested();

			var entities = await dbContext.Subtitles.AsNoTracking()
				.Where(x => x.SubtitleFileId == id)
				.ToListAsync(cancellationToken);

			return entities.Select(x => x.ToModel()).ToList();
		} catch (OperationCanceledException) {
			logger.LogWarning(LogEventIds.Subtitles, "Operation was cancelled");

			throw;
		}
	}

	public async Task<Subtitle> UpdateAsync(Subtitle subtitle, CancellationToken cancellationToken = default) {
		try {
			cancellationToken.ThrowIfCancellationRequested();

			var entity =
				await dbContext.Subtitles.SingleOrDefaultAsync(x => x.SubtitleId == subtitle.Id, cancellationToken);

			if (entity == null) {
				logger.LogWarning(LogEventIds.Subtitles, "Subtitle with id {Id} not found", subtitle.Id);

				throw new EntityNotFoundException<SubtitleEntity>(subtitle.Id);
			}

			entity.BlockId = subtitle.BlockId;
			entity.StartTime = subtitle.Start;
			entity.EndTime = subtitle.End;
			entity.Text = subtitle.Text;

			await dbContext.SaveChangesAsync(cancellationToken);

			return entity.ToModel();
		} catch (OperationCanceledException) {
			logger.LogWarning(LogEventIds.Subtitles, "Operation was cancelled");

			throw;
		}
	}

	public async Task DeleteAsync(int id, CancellationToken cancellationToken = default) {
		try {
			cancellationToken.ThrowIfCancellationRequested();

			var entity = await dbContext.Subtitles.SingleOrDefaultAsync(x => x.SubtitleId == id, cancellationToken);

			if (entity == null) {
				logger.LogWarning(LogEventIds.Subtitles, "Subtitle with id {Id} not found", id);

				throw new EntityNotFoundException<SubtitleEntity>(id);
			}

			dbContext.Subtitles.Remove(entity);
			await dbContext.SaveChangesAsync(cancellationToken);
		} catch (OperationCanceledException) {
			logger.LogWarning(LogEventIds.Subtitles, "Operation was cancelled");

			throw;
		}
	}
}
