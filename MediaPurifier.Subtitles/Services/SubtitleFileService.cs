using MediaPurifier.Data;
using MediaPurifier.Data.Entities;
using MediaPurifier.Data.Exceptions;
using MediaPurifier.Subtitles.Abstractions;
using MediaPurifier.Subtitles.Models;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;

namespace MediaPurifier.Subtitles.Services;

public class SubtitleFileService(
	ILogger<SubtitleFileService> logger,
	SubtitleParser parser,
	MediaPurifierContext dbContext
) : ISubtitleFileService {
	public IQueryable<SubtitleFile> QuerySubtitleFile() {
		return dbContext.SubtitleFiles
			.AsNoTracking()
			.Select(x => x.ToModel());
	}

	public async Task<SubtitleFile> AddAsync(
		int movieId, string file,
		CancellationToken cancellationToken = default
	) {
		try {
			cancellationToken.ThrowIfCancellationRequested();

			var movie = await dbContext.Movies.FirstOrDefaultAsync(m => m.MovieId == movieId, cancellationToken);

			if (movie == null) {
				logger.LogWarning(LogEventIds.Subtitles, "Movie with Id {Id} not found", movieId);

				throw new EntityNotFoundException<MovieEntity>(movieId);
			}

			var entity = new SubtitleFileEntity { SubtitleFile = file, MovieId = movie.MovieId };

			dbContext.SubtitleFiles.Add(entity);
			await dbContext.SaveChangesAsync(cancellationToken);

			return entity.ToModel();
		} catch (OperationCanceledException) {
			logger.LogWarning(LogEventIds.Subtitles, "Operation was cancelled");

			throw;
		}
	}

	public async Task<SubtitleFile> GetAsync(
		int id,
		CancellationToken cancellationToken = default
	) {
		try {
			cancellationToken.ThrowIfCancellationRequested();

			var subtitleFile = await dbContext.SubtitleFiles.Include(sf => sf.Movie)
				.FirstOrDefaultAsync(sf => sf.SubtitleFileId == id, cancellationToken);

			if (subtitleFile == null) {
				logger.LogWarning(LogEventIds.Subtitles, "Subtitle file with Id {Id} not found", id);

				throw new EntityNotFoundException<SubtitleFileEntity>(id);
			}

			return subtitleFile.ToModel();
		} catch (OperationCanceledException) {
			logger.LogWarning(LogEventIds.Subtitles, "Operation was cancelled");

			throw;
		}
	}

	public async Task<string[]> GetAsArrayAsync(int id, CancellationToken cancellationToken = default) {
		var file = await GetAsync(id, cancellationToken);

		return file.File.Split("\n");
	}

	public async Task<List<SubtitleFile>> GetByMovieIdAsync(
		int movieId,
		CancellationToken cancellationToken = default
	) {
		try {
			cancellationToken.ThrowIfCancellationRequested();

			var movie = await dbContext.Movies.FirstOrDefaultAsync(m => m.MovieId == movieId, cancellationToken);

			if (movie == null) {
				logger.LogWarning(LogEventIds.Subtitles, "Movie with Id {Id} not found", movieId);

				throw new EntityNotFoundException<MovieEntity>(movieId);
			}

			var subtitleFiles = await dbContext.SubtitleFiles.Where(sf => sf.MovieId == movie.MovieId)
				.ToListAsync(cancellationToken);

			return subtitleFiles.Select(sf => sf.ToModel()).ToList();
		} catch (OperationCanceledException) {
			logger.LogWarning(LogEventIds.Subtitles, "Operation was cancelled");

			throw;
		}
	}

	public async Task<List<SubtitleFile>> ListAsync(CancellationToken cancellationToken = default) {
		try {
			cancellationToken.ThrowIfCancellationRequested();

			var entities = await dbContext.SubtitleFiles.ToListAsync(cancellationToken);

			return entities
				.Select(sf => sf.ToModel())
				.ToList();
		} catch (OperationCanceledException) {
			logger.LogWarning(LogEventIds.Subtitles, "Operation was cancelled");

			throw;
		}
	}

	public async Task<SubtitleFile> UpdateAsync(
		SubtitleFile subtitleFile,
		CancellationToken cancellationToken = default
	) {
		try {
			cancellationToken.ThrowIfCancellationRequested();

			var entity =
				await dbContext.SubtitleFiles.SingleOrDefaultAsync(
					x => x.SubtitleFileId == subtitleFile.Id,
					cancellationToken
				);

			if (entity == null) {
				logger.LogWarning(LogEventIds.Subtitles, "Subtitle file with Id {Id} not found", subtitleFile.Id);

				throw new EntityNotFoundException<SubtitleFileEntity>(subtitleFile.Id);
			}

			entity.SubtitleFile = subtitleFile.File;
			entity.MovieId = subtitleFile.MovieId ?? 0;

			await dbContext.SaveChangesAsync(cancellationToken);

			return entity.ToModel();
		} catch (OperationCanceledException) {
			logger.LogWarning(LogEventIds.Subtitles, "Operation was cancelled");

			throw;
		}
	}

	public async Task<bool> DeleteAsync(int id, CancellationToken cancellationToken = default) {
		try {
			cancellationToken.ThrowIfCancellationRequested();
			var subtitleFile =
				await dbContext.SubtitleFiles.SingleOrDefaultAsync(x => x.SubtitleFileId == id, cancellationToken);

			if (subtitleFile == null) {
				logger.LogWarning(LogEventIds.Subtitles, "Subtitle file with Id {Id} not found", id);

				throw new EntityNotFoundException<SubtitleFileEntity>(id);
			}

			dbContext.SubtitleFiles.Remove(subtitleFile);
			await dbContext.SaveChangesAsync(cancellationToken);

			return true;
		} catch (OperationCanceledException) {
			logger.LogWarning(LogEventIds.Subtitles, "Operation was cancelled");

			throw;
		}
	}

	public async Task<bool> ParseFileAsync(int id, CancellationToken cancellationToken = default) {
		try {
			var subtitleFile = await GetAsArrayAsync(id, cancellationToken);

			var parsedSubtitles = parser.ParseSubtitleList(subtitleFile);

			var entities = parsedSubtitles.Select(x => x.ToEntity()).ToList();

			await dbContext.Subtitles.AddRangeAsync(entities, cancellationToken);
			await dbContext.SaveChangesAsync(cancellationToken);

			return true;
		} catch (OperationCanceledException) {
			logger.LogWarning(LogEventIds.Subtitles, "Operation was cancelled");

			throw;
		}
	}
}
