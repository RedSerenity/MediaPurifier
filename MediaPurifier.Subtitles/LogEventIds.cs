using Microsoft.Extensions.Logging;

namespace MediaPurifier.Subtitles;

public static class LogEventIds {
	public static readonly EventId Subtitles = 5060;
}
