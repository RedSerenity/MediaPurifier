namespace MediaPurifier.GraphQL.Types.Movies;

public record UpdateMovieInput(
	int Id,
	string Title,
	string? ImDbId,
	string? TvDbId
);
