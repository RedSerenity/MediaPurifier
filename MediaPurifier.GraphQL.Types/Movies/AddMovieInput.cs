namespace MediaPurifier.GraphQL.Types.Movies;

public record AddMovieInput(
	string Title,
	string? ImDbId,
	string? TvDbId
);
