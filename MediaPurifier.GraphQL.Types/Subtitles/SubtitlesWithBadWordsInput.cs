namespace MediaPurifier.GraphQL.Types.Subtitles;

public record SubtitlesWithBadWordsInput(
	int[] Ids
);
