namespace MediaPurifier.GraphQL.Types.Subtitles;

public record UpdateSubtitleFileInput(
	int Id,
	string Subtitle
);
