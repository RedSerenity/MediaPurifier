namespace MediaPurifier.GraphQL.Types.Subtitles;

public record AddSubtitleFileInput(
	int MovieId,
	string Subtitle
);
