namespace MediaPurifier.GraphQL.Types.Subtitles;

public record MarkSubtitleInput(
	int Id,
	bool HasBadWord
);
