#pragma warning disable CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.
namespace MediaPurifier.GraphQL.Types.Subtitles;

public class RawSubtitle {
	public int Id { get; init; }
	public int MovieId { get; init; }
	public string Subtitle { get; init; }
}
