namespace MediaPurifier.GraphQL.Types.Profanities;

public record AddProfanityInput(
	string Word,
	string MaskedWord
);
